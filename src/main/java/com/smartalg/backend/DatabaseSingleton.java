package com.smartalg.backend;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import com.smartalg.backend.Models.Database.Activity;
import com.smartalg.backend.Models.Database.Description;
import com.smartalg.backend.Models.Database.Fuel;
import com.smartalg.backend.Models.Database.GlobalDecision;
import com.smartalg.backend.Models.Database.Material;
import com.smartalg.backend.Models.Database.Plant;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Models.Database.Resource;
import com.smartalg.backend.Models.Database.ResourceType;
import com.smartalg.backend.Models.Database.Section;
import com.smartalg.backend.Models.Database.SkilledPerson;
import com.smartalg.backend.Models.Database.UnitOfMeasure;
import com.smartalg.backend.Repositories.GlobalDecisionRepo;

public class DatabaseSingleton {
	private static final String TAG = DatabaseSingleton.class.getSimpleName();
	private static DatabaseSingleton instance;
	private static Nitrite database;
	private static ObjectRepository<Project> projectRepo;
	private static ObjectRepository<Section> sectionRepo;
	private static ObjectRepository<Activity> activityRepo;
	private static ObjectRepository<Description> descriptionRepo;
	private static ObjectRepository<Resource> resourceRepo;
	private static ObjectRepository<ResourceType> resourceTypeRepo;
	private static ObjectRepository<UnitOfMeasure> unitOfMeasureRepo;
	private static ObjectRepository<GlobalDecision> globalDecisionRepo;
	private static ObjectRepository<Plant> plantRepo;
	private static ObjectRepository<SkilledPerson> skilledPersonRepo;
	private static ObjectRepository<Fuel> fuelRepo;
	private static ObjectRepository<Material> materialRepo;
	
	protected static DatabaseSingleton getDatabaseInstance(Nitrite database) {
		if(instance == null) {
			instance = new DatabaseSingleton();
			instance.database = database;
		}
		return instance;
	}

	/**
	 * @return the projectRepo
	 */
	public static ObjectRepository<Project> getProjectRepo() {
		if(projectRepo == null)
			projectRepo = database.getRepository(Project.class);
		return projectRepo;
	}

	/**
	 * @return the sectionRepo
	 */
	public static ObjectRepository<Section> getSectionRepo() {
		if(sectionRepo == null)
			sectionRepo = database.getRepository(Section.class);
		return sectionRepo;
	}

	/**
	 * @return the activityRepo
	 */
	public static ObjectRepository<Activity> getActivityRepo() {
		if(activityRepo == null)
			activityRepo = database.getRepository(Activity.class);
		return activityRepo;
	}

	/**
	 * @return the descriptionRepo
	 */
	public static ObjectRepository<Description> getDescriptionRepo() {
		if(descriptionRepo == null)
			descriptionRepo = database.getRepository(Description.class);
		return descriptionRepo;
	}

	/**
	 * @return the resourceRepo
	 */
	public static ObjectRepository<Resource> getResourceRepo() {
		if(resourceRepo == null)
			resourceRepo = database.getRepository(Resource.class);
		return resourceRepo;
	}

	/**
	 * @return the resourceTypeRepo
	 */
	public static ObjectRepository<ResourceType> getResourceTypeRepo() {
		if(resourceTypeRepo == null)
			resourceTypeRepo = database.getRepository(ResourceType.class);
		return resourceTypeRepo;
	}

	/**
	 * @return the unitOfMeasureRepo
	 */
	public static ObjectRepository<UnitOfMeasure> getUnitOfMeasureRepo() {
		if(unitOfMeasureRepo == null)
			unitOfMeasureRepo = database.getRepository(UnitOfMeasure.class);
		return unitOfMeasureRepo;
	}

	/**
	 * @return the globalDecisionRepo
	 */
	public static ObjectRepository<GlobalDecision> getGlobalDecisionRepo() {
		if(globalDecisionRepo == null)
			globalDecisionRepo = database.getRepository(GlobalDecision.class);
		return globalDecisionRepo;
	}

	/**
	 * @return the plantRepo
	 */
	public static ObjectRepository<Plant> getPlantRepo() {
		if(plantRepo == null) {
			plantRepo = database.getRepository(Plant.class);
		}
		return plantRepo;
	}

	/**
	 * @return the skilledPersonRepo
	 */
	public static ObjectRepository<SkilledPerson> getSkilledPersonRepo() {
		if(skilledPersonRepo == null) {
			skilledPersonRepo = database.getRepository(SkilledPerson.class);
		}
		return skilledPersonRepo;
	}

	/**
	 * @return the fuelRepo
	 */
	public static ObjectRepository<Fuel> getFuelRepo() {
		if(fuelRepo == null) {
			fuelRepo = database.getRepository(Fuel.class);
		}
		return fuelRepo;
	}

	/**
	 * @return the materialRepo
	 */
	public static ObjectRepository<Material> getMaterialRepo() {
		if(materialRepo == null) {
			materialRepo = database.getRepository(Material.class);
		}
		return materialRepo;
	}
	
	
}
