package com.smartalg.backend.Helpers;

import java.io.IOException;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class GeneralHelper {

	public static String getJsonFromObject(Object object){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new MyDateTypeAdapter())
                .create();
        return gson.toJson(object);
    }

    public static Object getObjectFromJson(String json, Class<?> classType){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new MyDateTypeAdapter())
                .create();
        return gson.fromJson(json,classType);
    }
    
    /*public static boolean isSubscribed(User user, Competition competition){
        for(Subscription subscriptionResponse : competition.getSubscriptions()){
            if(subscriptionResponse.getUser().getId().equals(user.getId()))
                return true;
        }
        return false;
    }
    
    public static Subscription getSubscription(User user, Competition competition){
        for(Subscription subscriptionResponse : competition.getSubscriptions()){
            if(subscriptionResponse.getUser().getId().equals(user.getId()))
                return subscriptionResponse;
        }
        return null;
    }*/
    
    public static class MyDateTypeAdapter extends TypeAdapter<Date> {
        @Override
        public void write(JsonWriter out, Date value) throws IOException {
            if (value == null)
                out.nullValue();
            else
                out.value(value.getTime() / 1000);
        }

        @Override
        public Date read(JsonReader in) throws IOException {
            if (in != null)
                return new Date(in.nextLong() * 1000);
            else
                return null;
        }
    }
}
