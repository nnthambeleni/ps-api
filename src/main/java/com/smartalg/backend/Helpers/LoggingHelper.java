package com.smartalg.backend.Helpers;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.smartalg.backend.App;

public class LoggingHelper {
	private static Logger logger = Logger.getLogger(LoggingHelper.class.getName());
	
	public static void d(String tag, String method, String message){
        if(App.LOGS_ENABLED) logger.logp(Level.WARNING, tag, method, message);
    }
    public static void i(String tag, String method, String message){
    	if(App.LOGS_ENABLED) logger.logp(Level.INFO, tag, method, message);
    }
    public static void e(String tag, String method, String message){
    	if(App.LOGS_ENABLED) logger.logp(Level.SEVERE, tag, method, message);
    }
    public static void v(String tag, String method, String message){
    	if(App.LOGS_ENABLED) logger.logp(Level.FINE, tag, method, message);
    }
}
