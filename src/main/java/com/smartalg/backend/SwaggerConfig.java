package com.smartalg.backend;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean(name="SwaggerConfig")
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
		          .select()
		          //.apis(RequestHandlerSelectors.basePackage("com.smartalg.controllers"))
		          .apis(RequestHandlerSelectors.any())
		          .paths(PathSelectors.any())
		          //.paths()
		          .build()
		          .apiInfo(apiInfo());
		          
	}
	
	private ApiInfo apiInfo() {
	     return new ApiInfo(
	       "PS REST API", 
	       "API To store data in DB.", 
	       "API TOS", 
	       "Terms of service", 
	       new Contact("Ndivhuwo", "www.smartalg.co.za", "info@smartalg.co.za"), 
	       "Apache License v2.0", "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	}
}
