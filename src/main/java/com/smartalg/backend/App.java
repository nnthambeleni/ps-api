package com.smartalg.backend;

import java.util.Arrays;

import javax.annotation.PreDestroy;

import org.dizitart.no2.Nitrite;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class App extends SpringBootServletInitializer
{
	private static Nitrite database;
	private static ApplicationContext ctx;
	public static boolean LOGS_ENABLED = true;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(App.class);
	}
	
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    }
    
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    	App.ctx = ctx;
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }

        };
    }
    
    @Bean(name="Database")
    public Nitrite initializeDatabase() {
    	try {
    	Nitrite db = Nitrite.builder()
    	        .compressed()
    	        .filePath("/var/ps/ps-database.db")
    	        .openOrCreate("ubuntu", "1183Nthambe");
    	database = db;
    	return db;
    	} catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    	
    }
    
    public static DatabaseSingleton getDBInstance() {
    	return DatabaseSingleton.getDatabaseInstance(database);
    }
    
    public static ApplicationContext getAppContext(){
        if (ctx == null){
            ctx = getAppContext();
        }
        return ctx;
    }
    
    @PreDestroy
    public void onDestroy() {
    	database.close();
    	database = null;
    	ctx = null;
    }
}
