package com.smartalg.backend.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Helpers.LoggingHelper;
import com.smartalg.backend.Models.Database.Activity;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Models.Database.Section;
import com.smartalg.backend.Models.Local.ListResponse;
import com.smartalg.backend.Repositories.ProjectRepo;
import com.smartalg.backend.Repositories.SectionRepo;

@RestController
public class SectionController {
	private static final String TAG = SectionController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/section/new", headers = "Accept=application/json")
	public ResponseEntity<?> putSection(@RequestParam("key") String key, @RequestBody String sectionJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			LoggingHelper.i(TAG, "putSection", "Valid Key entered, request body: " + sectionJSON);
			Section section = new Section();
			section = section.fromJson(sectionJSON);
			return SectionRepo.insertSection(section);
		}else {
			LoggingHelper.e(TAG, "putSection", "Invalid Key entered");
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/section/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllSections(@RequestParam("key") String key) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			LoggingHelper.i(TAG, "getAllSections", "Valid Key entered");
			return SectionRepo.getAllSections();
		}else {
			LoggingHelper.e(TAG, "getAllSections", "Invalid Key entered");
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/section/getByProjectId", headers = "Accept=application/json")
	public ResponseEntity<?> getAllSectionsByProjectId(@RequestParam("key") String key, @RequestParam("projectId") Long projectId) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			LoggingHelper.i(TAG, "getAllSections", "Valid Key entered");
			Project project = ProjectRepo.getActualProjectById(projectId);
			if(project != null) {
				List<Section> sections = new ArrayList<>();
				for(Activity activity : project.getActivities()) {
					if(!sections.contains(activity.getSection())) {
						sections.add(activity.getSection());
					}
				}
				return new ResponseEntity<>(new ListResponse(sections).toString(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Project not found", HttpStatus.NOT_FOUND);
			}
			
		}else {
			LoggingHelper.e(TAG, "getAllSections", "Invalid Key entered");
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
