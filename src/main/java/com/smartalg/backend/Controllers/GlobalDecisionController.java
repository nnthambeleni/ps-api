package com.smartalg.backend.Controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.GlobalDecision;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Repositories.GlobalDecisionRepo;
import com.smartalg.backend.Repositories.ProjectRepo;

@RestController
public class GlobalDecisionController {
	private static final String TAG = GlobalDecisionController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/globalDecision/new", headers = "Accept=application/json")
	public ResponseEntity<?> putGlobalDecision(@RequestParam("key") String key, @RequestParam(value="projectId") Long projectId, @RequestBody String globalDecisionJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			
			GlobalDecision globalDecision = new GlobalDecision();
			globalDecision = globalDecision.fromJson(globalDecisionJSON);
			GlobalDecision savedGlobalDecision = GlobalDecisionRepo.insertActualGlobalDecision(globalDecision);
			if(savedGlobalDecision != null) {
				Project project = ProjectRepo.getActualProjectById(projectId);
				if(project != null) {
					project.setGlobalDecision(savedGlobalDecision);
					return ProjectRepo.updateProject(project);
				}	else {
					return new ResponseEntity<>("Failed to find project with ID "+projectId+"", HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}else
				return new ResponseEntity<>("Failed to save globalDecision", HttpStatus.INTERNAL_SERVER_ERROR);
			
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
	
	
}
