package com.smartalg.backend.Controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Repositories.ProjectRepo;


@RestController
public class ProjectController {
	private static final String TAG = ProjectController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/project/new", headers = "Accept=application/json")
	public ResponseEntity<?> putProject(@RequestParam("key") String key, @RequestBody String projectJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			Project project = new Project();
			project = project.fromJson(projectJSON);
			return ProjectRepo.insertProject(project);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/project/update", headers = "Accept=application/json")
	public ResponseEntity<?> updateProject(@RequestParam("key") String key, @RequestBody String projectJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			Project project = new Project();
			project = project.fromJson(projectJSON);
			return ProjectRepo.updateProject(project);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/project/get_by_status", headers = "Accept=application/json")
	public ResponseEntity<?> getAllProjectsByStatus(@RequestParam("key") String key, @RequestParam(value="status") String status,
			@RequestParam(value="creator_id", required=false) Optional<Long> creator_id) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			return ProjectRepo.getProjectsByStatus(status, creator_id.isPresent() ? creator_id.get() : null);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/project/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllProjects(@RequestParam("key") String key,  @RequestParam(value="creator_id", required=false) Optional<Long> creator_id) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			return ProjectRepo.getAllProjects(creator_id.isPresent() ? creator_id.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.DELETE, path="/api/v1/project/delete/all", headers = "Accept=application/json")
	public ResponseEntity<?> deleteAllProjects(@RequestParam("key") String key){
		if(key.equals(Constants.REQUEST_KEY)) {
			return ProjectRepo.deleteAllProjects();
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
