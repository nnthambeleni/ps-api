package com.smartalg.backend.Controllers;


import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.Activity;
import com.smartalg.backend.Models.Database.Description;
import com.smartalg.backend.Models.Database.UnitOfMeasure;
import com.smartalg.backend.Models.Local.ListResponse;
import com.smartalg.backend.Repositories.ActivityRepo;
import com.smartalg.backend.Repositories.DescriptionRepo;
import com.smartalg.backend.Repositories.UnitOfMeasureRepo;

@RestController
public class DescriptionController {
	private static final String TAG = DescriptionController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/description/new", headers = "Accept=application/json")
	public ResponseEntity<?> postDescription(@RequestParam("key") String key, 
			@RequestParam(value="activityId") Long activityId, @RequestParam(value="unitOfMeasureId") Long unitOfMeasureId,
			@RequestBody String descriptionJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			UnitOfMeasure unitOfMeasure = UnitOfMeasureRepo.getActualUnitOfMeasureById(unitOfMeasureId);
			if(unitOfMeasure != null) {
				Description description = new Description();
				description = description.fromJson(descriptionJSON);
				description.setUnitOfMeasure(unitOfMeasure);
				Description savedDescription = DescriptionRepo.insertActualDescription(description);
				if(savedDescription != null) {
					Activity activity = ActivityRepo.getActualActivityById(activityId);
					activity.addDescription(savedDescription);
					return ActivityRepo.updateActivity(activity);
				}else
					return new ResponseEntity<>("Failed to save description", HttpStatus.INTERNAL_SERVER_ERROR);
			} else
				return new ResponseEntity<>("Selected unitOfMeasure not found", HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	//-------------------RESOURCES-------------------------
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/resource/add/{type}", headers = "Accept=application/json")
	public ResponseEntity<?> addResource(@RequestParam("key") String key, @RequestParam(value="descriptionId") Long descriptionId, @RequestParam(value="name") String name,  @RequestParam(value="count") int count, @PathVariable String type) {
		if(key.equals(Constants.REQUEST_KEY)) {
			Description description = DescriptionRepo.getActualDescriptionById(descriptionId);
			if(description != null) {
				switch(type) {
					case Constants.RESOURCE_TYPE_PEOPLE:
						description.addSkilledPeople(name, count);
						break;
					case Constants.RESOURCE_TYPE_FUEL:
						description.addFuel(name, count);
						break;
					case Constants.RESOURCE_TYPE_PLANT:
						description.addPlant(name, count);
						break;
					case Constants.RESOURCE_TYPE_MATERIAL:
						description.addMaterial(name, count);
						break;
				}
				return DescriptionRepo.updateDescription(description);
			}else {
				return new ResponseEntity<>("Description not found", HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/description/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllDescriptions(@RequestParam("key") String key,  
			@RequestParam(value="descriptionId", required=false) Optional<Long> descriptionId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return DescriptionRepo.getAllDescriptions(descriptionId.isPresent() ? descriptionId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/description/getById", headers = "Accept=application/json")
	public ResponseEntity<?> getDescriptionById(@RequestParam("key") String key,  
			@RequestParam("descriptionId") Long descriptionId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return DescriptionRepo.getAllDescriptions(descriptionId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/description/getByActivityId", headers = "Accept=application/json")
	public ResponseEntity<?> getDescriptionsByActivityId(@RequestParam("key") String key,  
			@RequestParam("activityId") Long activityId){
		if(key.equals(Constants.REQUEST_KEY)) {
			Activity activity  = ActivityRepo.getActualActivityById(activityId);
			if(activity != null) {
				return new ResponseEntity<>(new ListResponse(activity.getDescriptions()).toString(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("Activity not found", HttpStatus.NOT_FOUND);
			}
		
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
