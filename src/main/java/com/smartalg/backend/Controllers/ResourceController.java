package com.smartalg.backend.Controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.Description;
import com.smartalg.backend.Models.Database.Fuel;
import com.smartalg.backend.Models.Database.Material;
import com.smartalg.backend.Models.Database.Plant;
import com.smartalg.backend.Models.Database.Resource;
import com.smartalg.backend.Models.Database.ResourceType;
import com.smartalg.backend.Models.Database.SkilledPerson;
import com.smartalg.backend.Repositories.ActivityRepo;
import com.smartalg.backend.Repositories.DescriptionRepo;
import com.smartalg.backend.Repositories.FuelRepo;
import com.smartalg.backend.Repositories.MaterialRepo;
import com.smartalg.backend.Repositories.PlantRepo;
import com.smartalg.backend.Repositories.ResourceRepo;
import com.smartalg.backend.Repositories.ResourceTypeRepo;
import com.smartalg.backend.Repositories.SkilledPersonRepo;

@RestController
public class ResourceController {
	private static final String TAG = ResourceController.class.getSimpleName();
	
	//-------------------PLANT-------------------------
	
	/**
	 * 
	 * @param key
	 * @param plantJSON
	 * @return
	 * @throws Exception
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/plant/new", headers = "Accept=application/json")
	public ResponseEntity<?> postPlant(@RequestParam("key") String key, @RequestBody String plantJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
				Plant plant = new Plant();
				plant = plant.fromJson(plantJSON);
				return PlantRepo.insertPlant(plant);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param key
	 * @param plantId
	 * @return
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/plant/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllPlants(@RequestParam("key") String key,  @RequestParam(value="plantId", required=false) Optional<String> plantId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return PlantRepo.getAllPlants(plantId.isPresent() ? plantId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/plant/getById", headers = "Accept=application/json")
	public ResponseEntity<?> getPlantById(@RequestParam("key") String key,  @RequestParam("plantId") String plantId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return PlantRepo.getAllPlants(plantId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	/*@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/people/new", headers = "Accept=application/json")
	public ResponseEntity<?> postSkilledPerson(@RequestParam("key") String key, @RequestParam(value="descriptionId") Long descriptionId, @RequestBody String peopleJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
				SkilledPerson skilledPerson = new SkilledPerson();
				skilledPerson = skilledPerson.fromJson(peopleJSON);
				SkilledPerson savedSkilledPerson = SkilledPersonRepo.insertActualSkilledPerson(skilledPerson);
				if(savedSkilledPerson != null) {
					Description description = DescriptionRepo.getActualDescriptionById(descriptionId);
					description.addPlant(savedSkilledPerson);
					return DescriptionRepo.updateDescription(description);
				}else
					return new ResponseEntity<>("Failed to save plant", HttpStatus.INTERNAL_SERVER_ERROR);
		
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}*/
	
	//-------------------PEOPLE-------------------------
	
	/**
	 * 
	 * @param key
	 * @param peopleJSON
	 * @return
	 * @throws Exception
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/people/new", headers = "Accept=application/json")
	public ResponseEntity<?> postSkilledPerson(@RequestParam("key") String key, @RequestBody String peopleJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
				SkilledPerson skilledPerson = new SkilledPerson();
				skilledPerson = skilledPerson.fromJson(peopleJSON);
				return SkilledPersonRepo.insertSkilledPerson(skilledPerson);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param key
	 * @param personId
	 * @return
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/people/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllPersons(@RequestParam("key") String key,  @RequestParam(value="personId", required=false) Optional<String> personId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return SkilledPersonRepo.getAllSkilledPersons(personId.isPresent() ? personId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/people/getById", headers = "Accept=application/json")
	public ResponseEntity<?> getPersonById(@RequestParam("key") String key,  @RequestParam("personId") String personId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return SkilledPersonRepo.getAllSkilledPersons(personId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	//-------------------FUEL-------------------------
	
	/**
	 * 
	 * @param key
	 * @param fuelJSON
	 * @return
	 * @throws Exception
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/fuel/new", headers = "Accept=application/json")
	public ResponseEntity<?> postFuel(@RequestParam("key") String key, @RequestBody String fuelJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			Fuel fuel = new Fuel();
			fuel = fuel.fromJson(fuelJSON);
			return FuelRepo.insertFuel(fuel);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param key
	 * @param fuelId
	 * @return
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/fuel/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllFuels(@RequestParam("key") String key,  @RequestParam(value="fuelId", required=false) Optional<String> fuelId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return FuelRepo.getAllFuels(fuelId.isPresent() ? fuelId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/fuel/getById", headers = "Accept=application/json")
	public ResponseEntity<?> getFuelById(@RequestParam("key") String key,  @RequestParam("fuelId") String fuelId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return FuelRepo.getAllFuels(fuelId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	//-------------------MATERIAL-------------------------
	
	/**
	 * 
	 * @param key
	 * @param materialJSON
	 * @return
	 * @throws Exception
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/material/new", headers = "Accept=application/json")
	public ResponseEntity<?> postMaterial(@RequestParam("key") String key, @RequestBody String materialJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			Material material = new Material();
			material = material.fromJson(materialJSON);
			return MaterialRepo.insertMaterial(material);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param key
	 * @param materialId
	 * @return
	 */
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/material/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllMaterial(@RequestParam("key") String key,  @RequestParam(value="materialId", required=false) Optional<String> materialId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return MaterialRepo.getAllMaterials(materialId.isPresent() ? materialId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/material/getById", headers = "Accept=application/json")
	public ResponseEntity<?> getMaterialById(@RequestParam("key") String key,  @RequestParam("materialId") String materialId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return MaterialRepo.getAllMaterials(materialId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
