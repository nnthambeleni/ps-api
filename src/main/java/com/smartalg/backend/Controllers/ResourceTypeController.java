package com.smartalg.backend.Controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.ResourceType;
import com.smartalg.backend.Repositories.ResourceTypeRepo;

@RestController
public class ResourceTypeController {
	private static final String TAG = ResourceTypeController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/resourceType/new", headers = "Accept=application/json")
	public ResponseEntity<?> putResourceType(@RequestParam("key") String key, @RequestBody String resourceTypeJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			ResourceType resourceType = new ResourceType();
			resourceType = resourceType.fromJson(resourceTypeJSON);
			return ResourceTypeRepo.insertResourceType(resourceType);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/resourceType/update", headers = "Accept=application/json")
	public ResponseEntity<?> updateResourceType(@RequestParam("key") String key, @RequestBody String resourceTypeJSON) {
		if(key.equals(Constants.REQUEST_KEY)) {
			ResourceType resourceType = new ResourceType();
			resourceType = resourceType.fromJson(resourceTypeJSON);
			return ResourceTypeRepo.updateResourceType(resourceType);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/resourceType/get_by_status", headers = "Accept=application/json")
	public ResponseEntity<?> getAllCompetitionsByStatus(@RequestParam("key") String key,  @RequestParam(value="status", required=false) Optional<String> status){
		if(key.equals(Constants.REQUEST_KEY)) {
			return ResourceTypeRepo.getResourceTypesByStatus(status.isPresent() ? status.get() : null);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
