package com.smartalg.backend.Controllers;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.smartalg.backend.Constants;
import com.smartalg.backend.Models.Database.Activity;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Models.Database.Section;
import com.smartalg.backend.Models.Local.SectionActivity;
import com.smartalg.backend.Repositories.ActivityRepo;
import com.smartalg.backend.Repositories.ProjectRepo;
import com.smartalg.backend.Repositories.SectionRepo;

@RestController
public class ActivityController {
	private static final String TAG = ActivityController.class.getSimpleName();
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.POST, path="/api/v1/activity/new", headers = "Accept=application/json")
	public ResponseEntity<?> putActivity(@RequestParam("key") String key, @RequestParam(value="projectId") Long projectId, @RequestBody String sectionActivityJSON) throws Exception {
		if(key.equals(Constants.REQUEST_KEY)) {
			SectionActivity sectionActivity = new SectionActivity();
			sectionActivity = sectionActivity.fromJson(sectionActivityJSON);
			Section section = SectionRepo.getActualSectionBySectionId(sectionActivity.getSectionId());
			if(section != null) {
				Activity activity = new Activity();
				activity.setActivityName(sectionActivity.getActivityName());
				activity.setActivityId(sectionActivity.getActivityId());
				activity.setSection(section);
				Activity savedActivity = ActivityRepo.insertActualActivity(activity);
				if(savedActivity != null) {
					Project project = ProjectRepo.getActualProjectById(projectId);
					project.addActivity(savedActivity);
					return ProjectRepo.updateProject(project);
				}else
					return new ResponseEntity<>("Failed to save activity", HttpStatus.INTERNAL_SERVER_ERROR);
			} else
				return new ResponseEntity<>("Selected section not found", HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/activity/get", headers = "Accept=application/json")
	public ResponseEntity<?> getAllActivities(@RequestParam("key") String key,  @RequestParam(value="sectionId", required=false) Optional<String> sectionId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return ActivityRepo.getAllActivities(sectionId.isPresent() ? sectionId.get() : null);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/activity/getBySectionId", headers = "Accept=application/json")
	public ResponseEntity<?> getActivitiesBySectionId(@RequestParam("key") String key,  @RequestParam("sectionId") String sectionId){
		if(key.equals(Constants.REQUEST_KEY)) {
			return ActivityRepo.getAllActivities(sectionId);
		}else { 
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@CrossOrigin(origins = {"http://freeli.smartalg.co.za", "http://localhost:8383"})
	@RequestMapping(method=RequestMethod.GET, path="/api/v1/activity/get_single/{activityId}", headers = "Accept=application/json")
	public ResponseEntity<?> getActivityById(@RequestParam("key") String key, @PathVariable Long activityId) {
		if(key.equals(Constants.REQUEST_KEY)) {
			return ActivityRepo.getActivityById(activityId);
		}else {
			return new ResponseEntity<>("Request key Invalid", HttpStatus.BAD_REQUEST);
		}
	}
	
	@ExceptionHandler(Exception.class)
	 public ResponseEntity<?> handleException(Exception e, WebRequest request) {
		 e.printStackTrace();
		 request.getDescription(false);
	        return new ResponseEntity<>("There wa an exception: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	 }
}
