package com.smartalg.backend;

public class Constants {
	public static final String REQUEST_KEY = "ps_backendv1.0_api_key";
	public static final int PLANT_FUEL_DRY_RATE = 1;
	public static final int PLANT_FUEL_WET_RATE = 2;
	
	public static final String RESOURCE_TYPE_PEOPLE = "people";
	public static final String RESOURCE_TYPE_PLANT = "plant";
	public static final String RESOURCE_TYPE_MATERIAL = "material";
	public static final String RESOURCE_TYPE_FUEL = "fuel";
}
