package com.smartalg.backend.Models.Local;

import com.smartalg.backend.Helpers.GeneralHelper;

public class SectionActivity {
	private String sectionName;
	private String sectionId;
	private String activityName;
	private String activityId;
	
	/**
	 * 
	 */
	public SectionActivity() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param sectionName
	 * @param sectionId
	 * @param activityName
	 * @param activityId
	 */
	public SectionActivity(String sectionName, String sectionId, String activityName, String activityId) {
		this.sectionName = sectionName;
		this.sectionId = sectionId;
		this.activityName = activityName;
		this.activityId = activityId;
	}
	
	public SectionActivity fromJson(String s) {
        return (SectionActivity) GeneralHelper.getObjectFromJson(s, SectionActivity.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the sectionName
	 */
	public String getSectionName() {
		return sectionName;
	}

	/**
	 * @param sectionName the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	/**
	 * @return the sectionId
	 */
	public String getSectionId() {
		return sectionId;
	}

	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	/**
	 * @return the activityName
	 */
	public String getActivityName() {
		return activityName;
	}

	/**
	 * @param activityName the activityName to set
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	
}
