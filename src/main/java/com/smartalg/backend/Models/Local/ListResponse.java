package com.smartalg.backend.Models.Local;

import java.util.List;

import com.smartalg.backend.Helpers.GeneralHelper;

public class ListResponse {
	private List<?> list;

	/**
	 * @param competitionList
	 */
	public ListResponse(List<?> competitionList) {
		this.list = competitionList;
	}
	
	

	/**
	 * 
	 */
	public ListResponse() {
	}



	/**
	 * @return the list
	 */
	public List<?> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<?> list) {
		this.list = list;
	}
	
	
	public ListResponse fromJson(String s) {
        return (ListResponse) GeneralHelper.getObjectFromJson(s, ListResponse.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }
	
}
