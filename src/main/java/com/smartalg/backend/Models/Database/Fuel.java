package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "fuelName", type = IndexType.Unique)
})
public class Fuel {
	@Id
	private Long id;
	private String fuelName;
	private double rawRate;
	private double sellingRate;
	private double wastageFactor;
	private UnitOfMeasure unitOfMeasure;
	
	/**
	 * 
	 */
	public Fuel() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param fuelName
	 * @param rawRate
	 * @param sellingRate
	 * @param wastageFactor
	 * @param unitOfMeasure
	 */
	public Fuel(Long id, String fuelName, double rawRate, double sellingRate, double wastageFactor,
			UnitOfMeasure unitOfMeasure) {
		this.id = id;
		this.fuelName = fuelName;
		this.rawRate = rawRate;
		this.sellingRate = sellingRate;
		this.wastageFactor = wastageFactor;
		this.unitOfMeasure = unitOfMeasure;
	}
	
	public Fuel fromJson(String s) {
        return (Fuel) GeneralHelper.getObjectFromJson(s, Fuel.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);

	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the fuelName
	 */
	public String getFuelName() {
		return fuelName;
	}

	/**
	 * @param fuelName the fuelName to set
	 */
	public void setFuelName(String fuelName) {
		this.fuelName = fuelName;
	}

	/**
	 * @return the rawRate
	 */
	public double getRawRate() {
		return rawRate;
	}

	/**
	 * @param rawRate the rawRate to set
	 */
	public void setRawRate(double rawRate) {
		this.rawRate = rawRate;
	}

	/**
	 * @return the sellingRate
	 */
	public double getSellingRate() {
		return sellingRate;
	}

	/**
	 * @param sellingRate the sellingRate to set
	 */
	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	/**
	 * @return the wastageFactor
	 */
	public double getWastageFactor() {
		return wastageFactor;
	}

	/**
	 * @param wastageFactor the wastageFactor to set
	 */
	public void setWastageFactor(double wastageFactor) {
		this.wastageFactor = wastageFactor;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	
}
