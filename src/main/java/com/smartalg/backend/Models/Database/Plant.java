package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

import org.dizitart.no2.objects.Id;

@Indices({
	@Index(value = "plantName", type = IndexType.Unique)
})

public class Plant {
	@Id
	private Long id;
	private String plantName;
	private int plantFuel;
	private double rawRate;
	private double transportCost;
	private double sellingRate;
	private double wastageFactor;
	private double increase;
	private UnitOfMeasure unitOfMeasure;
	
	/**
	 * 
	 */
	public Plant() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param plantName
	 * @param plantFuel
	 * @param rawRate
	 * @param transportCost
	 * @param sellingRate
	 * @param wastageFactor
	 * @param increase
	 * @param unitOfMeasure
	 */
	public Plant(Long id, String plantName, int plantFuel, double rawRate, double transportCost, double sellingRate,
			double wastageFactor, double increase, UnitOfMeasure unitOfMeasure) {
		this.id = id;
		this.plantName = plantName;
		this.plantFuel = plantFuel;
		this.rawRate = rawRate;
		this.transportCost = transportCost;
		this.sellingRate = sellingRate;
		this.wastageFactor = wastageFactor;
		this.increase = increase;
		this.unitOfMeasure = unitOfMeasure;
	}

	public Plant fromJson(String s) {
        return (Plant) GeneralHelper.getObjectFromJson(s, Plant.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);

	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the plantName
	 */
	public String getPlantName() {
		return plantName;
	}

	/**
	 * @param plantName the plantName to set
	 */
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	/**
	 * @return the plantFuel
	 */
	public int getPlantFuel() {
		return plantFuel;
	}

	/**
	 * @param plantFuel the plantFuel to set
	 */
	public void setPlantFuel(int plantFuel) {
		this.plantFuel = plantFuel;
	}

	/**
	 * @return the rawRate
	 */
	public double getRawRate() {
		return rawRate;
	}

	/**
	 * @param rawRate the rawRate to set
	 */
	public void setRawRate(double rawRate) {
		this.rawRate = rawRate;
	}

	/**
	 * @return the transportCost
	 */
	public double getTransportCost() {
		return transportCost;
	}

	/**
	 * @param transportCost the transportCost to set
	 */
	public void setTransportCost(double transportCost) {
		this.transportCost = transportCost;
	}

	/**
	 * @return the sellingRate
	 */
	public double getSellingRate() {
		return sellingRate;
	}

	/**
	 * @param sellingRate the sellingRate to set
	 */
	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	/**
	 * @return the wastageFactor
	 */
	public double getWastageFactor() {
		return wastageFactor;
	}

	/**
	 * @param wastageFactor the wastageFactor to set
	 */
	public void setWastageFactor(double wastageFactor) {
		this.wastageFactor = wastageFactor;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	/**
	 * @return the increase
	 */
	public double getIncrease() {
		return increase;
	}

	/**
	 * @param increase the increase to set
	 */
	public void setIncrease(double increase) {
		this.increase = increase;
	}
	
	
	
}
