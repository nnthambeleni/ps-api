package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "typeName", type = IndexType.Unique)
})
public class ResourceType {
	@Id
	private Long id;
	private String typeName;
	private String rateType;
	private String currency;
	private boolean active;
	
	/**
	 * 
	 */
	public ResourceType() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id
	 * @param typeName
	 * @param rateType
	 * @param currency
	 * @param active
	 */
	public ResourceType(Long id, String typeName, String rateType, String currency, boolean active) {
		this.id = id;
		this.typeName = typeName;
		this.rateType = rateType;
		this.currency = currency;
		this.active = active;
	}
	
	public ResourceType fromJson(String s) {
        return (ResourceType) GeneralHelper.getObjectFromJson(s, ResourceType.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the rateType
	 */
	public String getRateType() {
		return rateType;
	}

	/**
	 * @param rateType the rateType to set
	 */
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
