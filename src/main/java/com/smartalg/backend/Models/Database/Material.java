package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "materialName", type = IndexType.Unique)
})
public class Material {
	@Id
	private Long id;
	private String materialName;
	private double rawRate;
	private double sellingRate;
	private double wastageFactor;
	private double increase;
	private UnitOfMeasure unitOfMeasure;
	
	/**
	 * 
	 */
	public Material() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param materialName
	 * @param rawRate
	 * @param sellingRate
	 * @param wastageFactor
	 * @param increase
	 * @param unitOfMeasure
	 */
	public Material(Long id, String materialName, double rawRate, double sellingRate, double wastageFactor,
			double increase, UnitOfMeasure unitOfMeasure) {
		this.id = id;
		this.materialName = materialName;
		this.rawRate = rawRate;
		this.sellingRate = sellingRate;
		this.wastageFactor = wastageFactor;
		this.increase = increase;
		this.unitOfMeasure = unitOfMeasure;
	}

	public Material fromJson(String s) {
        return (Material) GeneralHelper.getObjectFromJson(s, Material.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);

	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the materialName
	 */
	public String getMaterialName() {
		return materialName;
	}

	/**
	 * @param materialName the materialName to set
	 */
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	/**
	 * @return the rawRate
	 */
	public double getRawRate() {
		return rawRate;
	}

	/**
	 * @param rawRate the rawRate to set
	 */
	public void setRawRate(double rawRate) {
		this.rawRate = rawRate;
	}

	/**
	 * @return the sellingRate
	 */
	public double getSellingRate() {
		return sellingRate;
	}

	/**
	 * @param sellingRate the sellingRate to set
	 */
	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	/**
	 * @return the wastageFactor
	 */
	public double getWastageFactor() {
		return wastageFactor;
	}

	/**
	 * @param wastageFactor the wastageFactor to set
	 */
	public void setWastageFactor(double wastageFactor) {
		this.wastageFactor = wastageFactor;
	}

	/**
	 * @return the increase
	 */
	public double getIncrease() {
		return increase;
	}

	/**
	 * @param increase the increase to set
	 */
	public void setIncrease(double increase) {
		this.increase = increase;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	
}
