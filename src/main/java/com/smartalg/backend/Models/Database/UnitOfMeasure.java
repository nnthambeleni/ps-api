package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "unitAbbreviation", type = IndexType.Unique)
})
public class UnitOfMeasure {
	@Id
	private Long id;
	private String unitName;
	private String unitAbbreviation;
	private boolean active;
	
	/**
	 * 
	 */
	public UnitOfMeasure() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id
	 * @param unitName
	 * @param unitAbbreviation
	 * @param active
	 */
	public UnitOfMeasure(Long id, String unitName, String unitAbbreviation, boolean active) {
		this.id = id;
		this.unitName = unitName;
		this.unitAbbreviation = unitAbbreviation;
		this.active = active;
	}

	public UnitOfMeasure fromJson(String s) {
        return (UnitOfMeasure) GeneralHelper.getObjectFromJson(s, UnitOfMeasure.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the unitName
	 */
	public String getUnitName() {
		return unitName;
	}

	/**
	 * @param unitName the unitName to set
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	/**
	 * @return the unitAbbreviation
	 */
	public String getUnitAbbreviation() {
		return unitAbbreviation;
	}

	/**
	 * @param unitAbbreviation the unitAbbreviation to set
	 */
	public void setUnitAbbreviation(String unitAbbreviation) {
		this.unitAbbreviation = unitAbbreviation;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
