package com.smartalg.backend.Models.Database;

import java.util.Date;
import java.util.List;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "sectionId", type = IndexType.Unique)
})
public class Section {
	@Id
	private Long id;
	private String sectionName;
	private String sectionId;
	private Date created;
	
	/**
	 * 
	 */
	public Section() {
		created = new Date();
	}
	
	/**
	 * @param id
	 * @param sectionName
	 * @param sectionId
	 * @param created
	 */
	public Section(Long id, String sectionName, String sectionId, Date created) {
		this.id = id;
		this.sectionName = sectionName;
		this.sectionId = sectionId;
		this.created = created;
	}

	public Section fromJson(String s) {
        return (Section) GeneralHelper.getObjectFromJson(s, Section.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sectionName
	 */
	public String getSectionName() {
		return sectionName;
	}

	/**
	 * @param sectionName the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	/**
	 * @return the sectionId
	 */
	public String getSectionId() {
		return sectionId;
	}

	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

}
