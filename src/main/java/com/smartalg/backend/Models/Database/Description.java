package com.smartalg.backend.Models.Database;

import java.util.HashMap;
import java.util.List;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "descriptionName", type = IndexType.Unique),
	@Index(value = "id", type = IndexType.Unique)
})
public class Description {
	@Id
	private Long id;
	private String descriptionName;
	private String productionAcheivable;
	private Long totalQuantity;
	private UnitOfMeasure unitOfMeasure;
	private HashMap<String, Integer> plant;
	private HashMap<String, Integer> skilledPeople;
	private HashMap<String, Integer> fuel;
	private HashMap<String, Integer> material;
	
	/**
	 * 
	 */
	public Description() {
		// TODO Auto-generated constructor stub
	}
	
	public Description fromJson(String s) {
        return (Description) GeneralHelper.getObjectFromJson(s, Description.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the descriptionName
	 */
	public String getDescriptionName() {
		return descriptionName;
	}

	/**
	 * @param descriptionName the descriptionName to set
	 */
	public void setDescriptionName(String descriptionName) {
		this.descriptionName = descriptionName;
	}

	/**
	 * @return the productionAcheivable
	 */
	public String getProductionAcheivable() {
		return productionAcheivable;
	}

	/**
	 * @param productionAcheivable the productionAcheivable to set
	 */
	public void setProductionAcheivable(String productionAcheivable) {
		this.productionAcheivable = productionAcheivable;
	}

	/**
	 * @return the totalQuantity
	 */
	public Long getTotalQuantity() {
		return totalQuantity;
	}

	/**
	 * @param totalQuantity the totalQuantity to set
	 */
	public void setTotalQuantity(Long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	/**
	 * @return the plant
	 */
	public HashMap<String, Integer> getPlant() {
		return plant;
	}

	/**
	 * 
	 * @param plantName
	 * @param counter
	 */
	public void addPlant(String plantName, int counter) {
		if(plant == null) {
			plant = new HashMap<>();
		}
		
		if(!plant.containsKey(plantName)) {
			plant.put(plantName, counter);
		} else {
			plant.put(plantName, plant.get(plantName) + 1);
		}
		
	}

	/**
	 * @return the skilledPeople
	 */
	public HashMap<String, Integer> getSkilledPeople() {
		return skilledPeople;
	}

	/**
	 * 
	 * @param skilledPeopleName
	 * @param counter
	 */
	public void addSkilledPeople(String skilledPeopleName, int counter) {
		if(skilledPeople == null) {
			skilledPeople = new HashMap<>();
		}
		
		if(!skilledPeople.containsKey(skilledPeopleName)) {
			skilledPeople.put(skilledPeopleName, counter);
		} else {
			skilledPeople.put(skilledPeopleName, skilledPeople.get(skilledPeopleName) + 1);
		}
	}

	/**
	 * @return the fuel
	 */
	public HashMap<String, Integer> getFuel() {
		return fuel;
	}

	/**
	 * 
	 * @param fuelName
	 * @param counter
	 */
	public void addFuel(String fuelName, int counter) {
		if(fuel == null) {
			fuel = new HashMap<>();
		}
		
		if(!fuel.containsKey(fuelName)) {
			fuel.put(fuelName, counter);
		} else {
			fuel.put(fuelName, fuel.get(fuelName) + 1);
		}
	}

	/**
	 * @return the material
	 */
	public HashMap<String, Integer> getMaterial() {
		return material;
	}

	/**
	 * 
	 * @param materialName
	 * @param counter
	 */
	public void addMaterial(String materialName, int counter) {
		if(material == null) {
			material = new HashMap<>();
		}
		
		if(!material.containsKey(materialName)) {
			material.put(materialName, counter);
		} else {
			material.put(materialName, material.get(materialName) + 1);
		}
	}

	

}
