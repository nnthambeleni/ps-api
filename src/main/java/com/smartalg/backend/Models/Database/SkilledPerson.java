package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "personName", type = IndexType.Unique)
})
public class SkilledPerson {
	@Id
	private Long id;
	private String personName;
	private double salary;
	private double increase;
	private double levies;
	private double leave;
	private double pension;
	private double medical;
	private double bonus;
	private double totalCost;
	private UnitOfMeasure unitOfMeasure;
	
	/**
	 * 
	 */
	public SkilledPerson() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id
	 * @param personName
	 * @param salary
	 * @param increase
	 * @param levies
	 * @param leave
	 * @param pension
	 * @param medical
	 * @param bonus
	 * @param totalCost
	 * @param unitOfMeasure
	 */
	public SkilledPerson(Long id, String personName, double salary, double increase, double levies, double leave,
			double pension, double medical, double bonus, double totalCost, UnitOfMeasure unitOfMeasure) {
		this.id = id;
		this.personName = personName;
		this.salary = salary;
		this.increase = increase;
		this.levies = levies;
		this.leave = leave;
		this.pension = pension;
		this.medical = medical;
		this.bonus = bonus;
		this.totalCost = totalCost;
		this.unitOfMeasure = unitOfMeasure;
	}

	public SkilledPerson fromJson(String s) {
        return (SkilledPerson) GeneralHelper.getObjectFromJson(s, SkilledPerson.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);

	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the personName
	 */
	public String getPersonName() {
		return personName;
	}

	/**
	 * @param personName the personName to set
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}

	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}

	/**
	 * @return the increase
	 */
	public double getIncrease() {
		return increase;
	}

	/**
	 * @param increase the increase to set
	 */
	public void setIncrease(double increase) {
		this.increase = increase;
	}

	/**
	 * @return the levies
	 */
	public double getLevies() {
		return levies;
	}

	/**
	 * @param levies the levies to set
	 */
	public void setLevies(double levies) {
		this.levies = levies;
	}

	/**
	 * @return the leave
	 */
	public double getLeave() {
		return leave;
	}

	/**
	 * @param leave the leave to set
	 */
	public void setLeave(double leave) {
		this.leave = leave;
	}

	/**
	 * @return the pension
	 */
	public double getPension() {
		return pension;
	}

	/**
	 * @param pension the pension to set
	 */
	public void setPension(double pension) {
		this.pension = pension;
	}

	/**
	 * @return the medical
	 */
	public double getMedical() {
		return medical;
	}

	/**
	 * @param medical the medical to set
	 */
	public void setMedical(double medical) {
		this.medical = medical;
	}

	/**
	 * @return the bonus
	 */
	public double getBonus() {
		return bonus;
	}

	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	/**
	 * @return the totalCost
	 */
	public double getTotalCost() {
		return totalCost;
	}

	/**
	 * @param totalCost the totalCost to set
	 */
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	/**
	 * @return the unitOfMeasure
	 */
	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}
	
	
}
