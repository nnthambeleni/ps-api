package com.smartalg.backend.Models.Database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dizitart.no2.objects.Id;

import com.smartalg.backend.Helpers.GeneralHelper;

public class GlobalDecision {
//Overheads Percentage, Profit Percetage, Start Date, End date, Working hours per day
// Working days, project weekend, non-working days.
	@Id
	private Long id;
	private double overheardsPercentage;
	private double profitPercentage;
	private double workingHoursPerDay;
	private Date startDate;
	private Date endDate;
	private List<Date> workingDays;
	private List<Date> projectWeekendDays;
	private List<Date> nonWorkingDays;
	
	/**
	 * 
	 */
	public GlobalDecision() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id
	 * @param overheardsPercentage
	 * @param profitPercentage
	 * @param workingHoursPerDay
	 * @param startDate
	 * @param endDate
	 * @param workingDays
	 * @param projectWeekendDays
	 * @param nonWorkingDays
	 */
	public GlobalDecision(Long id, double overheardsPercentage, double profitPercentage, double workingHoursPerDay,
			Date startDate, Date endDate, List<Date> workingDays, List<Date> projectWeekendDays,
			List<Date> nonWorkingDays) {
		this.id = id;
		this.overheardsPercentage = overheardsPercentage;
		this.profitPercentage = profitPercentage;
		this.workingHoursPerDay = workingHoursPerDay;
		this.startDate = startDate;
		this.endDate = endDate;
		this.workingDays = workingDays;
		this.projectWeekendDays = projectWeekendDays;
		this.nonWorkingDays = nonWorkingDays;
	}
	
	public GlobalDecision fromJson(String s) {
        return (GlobalDecision) GeneralHelper.getObjectFromJson(s, GlobalDecision.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the overheardsPercentage
	 */
	public double getOverheardsPercentage() {
		return overheardsPercentage;
	}

	/**
	 * @param overheardsPercentage the overheardsPercentage to set
	 */
	public void setOverheardsPercentage(double overheardsPercentage) {
		this.overheardsPercentage = overheardsPercentage;
	}

	/**
	 * @return the profitPercentage
	 */
	public double getProfitPercentage() {
		return profitPercentage;
	}

	/**
	 * @param profitPercentage the profitPercentage to set
	 */
	public void setProfitPercentage(double profitPercentage) {
		this.profitPercentage = profitPercentage;
	}

	/**
	 * @return the workingHoursPerDay
	 */
	public double getWorkingHoursPerDay() {
		return workingHoursPerDay;
	}

	/**
	 * @param workingHoursPerDay the workingHoursPerDay to set
	 */
	public void setWorkingHoursPerDay(double workingHoursPerDay) {
		this.workingHoursPerDay = workingHoursPerDay;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param eandDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the workingDays
	 */
	public List<Date> getWorkingDays() {
		return workingDays;
	}

	/**
	 * @param workingDays the workingDays to set
	 */
	public void setWorkingDays(List<Date> workingDays) {
		this.workingDays = workingDays;
	}

	/**
	 * @return the projectWeekendDays
	 */
	public List<Date> getProjectWeekendDays() {
		return projectWeekendDays;
	}

	/**
	 * @param projectWeekendDays the projectWeekendDays to set
	 */
	public void setProjectWeekendDays(List<Date> projectWeekendDays) {
		
		this.projectWeekendDays = projectWeekendDays;
	}

	/**
	 * @return the nonWorkingDays
	 */
	public List<Date> getNonWorkingDays() {
		return nonWorkingDays;
	}

	/**
	 * @param nonWorkingDays the nonWorkingDays to set
	 */
	public void setNonWorkingDays(List<Date> nonWorkingDays) {
		this.nonWorkingDays = nonWorkingDays;
	}
	
	
}
