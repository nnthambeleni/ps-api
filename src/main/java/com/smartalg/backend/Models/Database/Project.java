package com.smartalg.backend.Models.Database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "id", type = IndexType.Unique),
	@Index(value = "projectNumber", type = IndexType.Unique)
})
public class Project {
	@Id
	private Long id;
	private String projectName;
	private String projectNumber;
	private String clientName;
	private Date created;
	private boolean active;
	private List<Activity> activities;
	private GlobalDecision globalDecision;
	
	/**
	 * 
	 */
	public Project() {
		created = new Date();
	}

	/**
	 * @param id
	 * @param projectName
	 * @param projectNumber
	 * @param clientName
	 * @param created
	 * @param active
	 * @param activities
	 * @param globalDecision
	 */
	public Project(Long id, String projectName, String projectNumber, String clientName, Date created, boolean active,
			List<Activity> activities, GlobalDecision globalDecision) {
		this.id = id;
		this.projectName = projectName;
		this.projectNumber = projectNumber;
		this.clientName = clientName;
		this.created = created;
		this.active = active;
		this.activities = activities;
		this.globalDecision = globalDecision;
	}

	public Project fromJson(String s) {
        return (Project) GeneralHelper.getObjectFromJson(s, Project.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the projectNumber
	 */
	public String getProjectNumber() {
		return projectNumber;
	}

	/**
	 * @param projectNumber the projectNumber to set
	 */
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientName the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the activities
	 */
	public List<Activity> getActivities() {
		return activities;
	}

	/**
	 * @param activities the activities to set
	 */
	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}

	/**
	 * @param add activity to the activities list
	 */
	public void addActivity(Activity activity) {
		if(activities == null)
			activities = new ArrayList<>();
		this.activities.add(activity);
	}

	/**
	 * @return the globalDecision
	 */
	public GlobalDecision getGlobalDecision() {
		return globalDecision;
	}

	/**
	 * @param globalDecision the globalDecision to set
	 */
	public void setGlobalDecision(GlobalDecision globalDecision) {
		this.globalDecision = globalDecision;
	}
	
	
}
