package com.smartalg.backend.Models.Database;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "resourceName", type = IndexType.Unique)
})
public class Resource {
	@Id
	private Long id;
	private String resourceName;
	private ResourceType resourceType;
	private int quantity;
	private double rate;
	private double wastageFactor;
	
	/**
	 * 
	 */
	public Resource() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param resourceName
	 * @param resourceType
	 * @param quantity
	 * @param rate
	 * @param wastageFactor
	 */
	public Resource(Long id, String resourceName, ResourceType resourceType, int quantity, double rate,
			double wastageFactor) {
		this.id = id;
		this.resourceName = resourceName;
		this.resourceType = resourceType;
		this.quantity = quantity;
		this.rate = rate;
		this.wastageFactor = wastageFactor;
	}
	
	public Resource fromJson(String s) {
        return (Resource) GeneralHelper.getObjectFromJson(s, Resource.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the resourceType
	 */
	public ResourceType getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType the resourceType to set
	 */
	public void setResourceType(ResourceType resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}

	/**
	 * @return the wastageFactor
	 */
	public double getWastageFactor() {
		return wastageFactor;
	}

	/**
	 * @param wastageFactor the wastageFactor to set
	 */
	public void setWastageFactor(double wastageFactor) {
		this.wastageFactor = wastageFactor;
	}
	
	
}
