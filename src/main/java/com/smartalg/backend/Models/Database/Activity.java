package com.smartalg.backend.Models.Database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;

import com.smartalg.backend.Helpers.GeneralHelper;

@Indices({
	@Index(value = "activityId", type = IndexType.Unique),
	@Index(value = "id", type = IndexType.Unique)
})
public class Activity {
	@Id
	private Long id;
	private String activityName;
	private String activityId;
	private Date created;
	private List<Description> descriptions;
	private Section section;
	
	/**
	 * 
	 */
	public Activity() {
		created = new Date();
	}
	
	/**
	 * @param id
	 * @param activityName
	 * @param activityId
	 * @param created
	 * @param descriptions
	 * @param section
	 */
	public Activity(Long id, String activityName, String activityId, Date created, List<Description> descriptions,
			Section section) {
		this.id = id;
		this.activityName = activityName;
		this.activityId = activityId;
		this.created = created;
		this.descriptions = descriptions;
		this.section = section;
	}

	public Activity fromJson(String s) {
        return (Activity) GeneralHelper.getObjectFromJson(s, Activity.class);
    }

	public String toString() {
        return GeneralHelper.getJsonFromObject(this);
    }

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the activityName
	 */
	public String getActivityName() {
		return activityName;
	}

	/**
	 * @param activityName the activityName to set
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	/**
	 * @return the activityId
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the descriptions
	 */
	public List<Description> getDescriptions() {
		return descriptions;
	}

	/**
	 * @param descriptions the descriptions to set
	 */
	public void setDescriptions(List<Description> descriptions) {
		this.descriptions = descriptions;
	}
	
	/**
	 * @param descriptions the descriptions to set
	 */
	public void addDescription(Description description) {
		if(descriptions == null) {
			descriptions = new ArrayList<>();
		}
		this.descriptions.add(description);
	}

	/**
	 * @return the section
	 */
	public Section getSection() {
		return section;
	}

	/**
	 * @param section the section to set
	 */
	public void setSection(Section section) {
		this.section = section;
	}
	
}
