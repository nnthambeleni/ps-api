package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Resource;


public class ResourceRepo {
	private static final String TAG = ResourceRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertResource(Resource resource) {
		resource.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(resource);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(resource.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save resource", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Resource insertActualResource(Resource resource) {
		resource.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(resource);
    	if(result != null && result.getAffectedCount() > 0) {
    		return resource;
    	}else {
    		return null;
    	}
	}
	
	public static Resource getActualResourceById(Long resourceId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Resource, ObjectFilters.eq("id", resourceId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Resource)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updateResource(Resource resource) {
		WriteResult result = RepoInteractor.updateEntry(resource);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(resource.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update resource", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}

}
