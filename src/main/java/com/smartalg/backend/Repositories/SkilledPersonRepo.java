package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.SkilledPerson;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class SkilledPersonRepo {
	private static final String TAG = SkilledPersonRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertSkilledPerson(SkilledPerson skilledPerson) {
		skilledPerson.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(skilledPerson);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(skilledPerson.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save SkilledPerson", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static SkilledPerson insertActualSkilledPerson(SkilledPerson SkilledPerson) {
		SkilledPerson.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(SkilledPerson);
    	if(result != null && result.getAffectedCount() > 0) {
    		return SkilledPerson;
    	}else {
    		return null;
    	}
	}
	
	public static SkilledPerson getActualSkilledPersonById(Long skilledPersonId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.SkilledPerson, ObjectFilters.eq("id", skilledPersonId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (SkilledPerson)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updateSkilledPerson(SkilledPerson skilledPerson) {
		WriteResult result = RepoInteractor.updateEntry(skilledPerson);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(skilledPerson.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update SkilledPerson", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getAllSkilledPersons(@Nullable String skilledPersonId) {
		Optional<List<?>> optional;
		if(skilledPersonId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.SkilledPerson, ObjectFilters.eq("id", skilledPersonId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.SkilledPerson);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}

}

