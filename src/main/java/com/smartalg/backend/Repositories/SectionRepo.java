package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Helpers.LoggingHelper;
import com.smartalg.backend.Models.Database.Section;
import com.smartalg.backend.Models.Local.ListResponse;


public class SectionRepo {
	private static final String TAG = SectionRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertSection(Section section) {
		
		section.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(section);
    	if(result != null && result.getAffectedCount() > 0) {
    		LoggingHelper.i(TAG, "insertSection", "Successfully Added asection");
    		return new ResponseEntity<>(section.toString(), HttpStatus.OK);
    	}else {
    		LoggingHelper.i(TAG, "insertSection", "Failed to save section");
    		return new ResponseEntity<>("Failed to save section", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Section insertActualSection(Section section) {
		section.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(section);
    	if(result != null && result.getAffectedCount() > 0) {
    		return section;
    	}else {
    		return null;
    	}
	}
	
	public static ResponseEntity<?>  updateSection(Section section) {
		WriteResult result = RepoInteractor.updateEntry(section);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(section.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update section", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getAllSections() {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Section);
	
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static Section getActualSectionBySectionId(String sectionId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Section, ObjectFilters.eq("sectionId", sectionId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Section)result.get(0) : null;
	}
}
