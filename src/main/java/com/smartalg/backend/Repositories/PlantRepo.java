package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Plant;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class PlantRepo {
	private static final String TAG = PlantRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertPlant(Plant plant) {
		plant.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(plant);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(plant.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save Plant", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Plant insertActualPlant(Plant Plant) {
		Plant.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(Plant);
    	if(result != null && result.getAffectedCount() > 0) {
    		return Plant;
    	}else {
    		return null;
    	}
	}
	
	public static Plant getActualPlantById(Long plantId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Plant, ObjectFilters.eq("id", plantId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Plant)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updatePlant(Plant plant) {
		WriteResult result = RepoInteractor.updateEntry(plant);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(plant.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update Plant", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getAllPlants(@Nullable String plantId) {
		Optional<List<?>> optional;
		if(plantId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Plant, ObjectFilters.eq("id", plantId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Plant);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}

}

