package com.smartalg.backend.Repositories;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.GlobalDecision;

public class GlobalDecisionRepo {
	private static final String TAG = GlobalDecisionRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertGlobalDecision(GlobalDecision globalDecision) {
		globalDecision.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(globalDecision);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(globalDecision.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save globalDecision", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static GlobalDecision insertActualGlobalDecision(GlobalDecision globalDecision) {
		globalDecision.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(globalDecision);
    	if(result != null && result.getAffectedCount() > 0) {
    		return globalDecision;
    	}else {
    		return null;
    	}
	}
}
