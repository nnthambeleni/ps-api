package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.UnitOfMeasure;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;

public class UnitOfMeasureRepo {
	private static final String TAG = UnitOfMeasureRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		unitOfMeasure.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(unitOfMeasure);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(unitOfMeasure.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save unitOfMeasure", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  updateUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		WriteResult result = RepoInteractor.updateEntry(unitOfMeasure);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(unitOfMeasure.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update unitOfMeasure", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getUnitOfMeasuresByStatus(@Nullable String status) {
		Optional<List<?>> optional;
		if(status != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.UnitOfMeasure, ObjectFilters.eq("active", status.equalsIgnoreCase("active")));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.UnitOfMeasure);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static UnitOfMeasure getActualUnitOfMeasureById(Long unitOfMeasureId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.UnitOfMeasure, ObjectFilters.eq("id", unitOfMeasureId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (UnitOfMeasure)result.get(0) : null;
	}
}
