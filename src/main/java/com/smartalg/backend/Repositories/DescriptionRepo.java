package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Description;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class DescriptionRepo {
	private static final String TAG = DescriptionRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertDescription(Description description) {
		description.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(description);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(description.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save description", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Description insertActualDescription(Description description) {
		description.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(description);
    	if(result != null && result.getAffectedCount() > 0) {
    		return description;
    	}else {
    		return null;
    	}
	}
	
	public static ResponseEntity<?>  getAllDescriptions(@Nullable Long descriptionId) {
		Optional<List<?>> optional;
		if(descriptionId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Description, ObjectFilters.eq("id", descriptionId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Description);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static Description getActualDescriptionById(Long descriptionId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Description, ObjectFilters.eq("id", descriptionId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Description)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updateDescription(Description description) {
		WriteResult result = RepoInteractor.updateEntry(description);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(description.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update description", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
}
