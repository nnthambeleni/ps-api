package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Material;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class MaterialRepo {
	private static final String TAG = MaterialRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertMaterial(Material material) {
		material.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(material);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(material.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save Material", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Material insertActualMaterial(Material Material) {
		Material.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(Material);
    	if(result != null && result.getAffectedCount() > 0) {
    		return Material;
    	}else {
    		return null;
    	}
	}
	
	public static Material getActualMaterialById(Long materialId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Material, ObjectFilters.eq("id", materialId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Material)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updateMaterial(Material material) {
		WriteResult result = RepoInteractor.updateEntry(material);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(material.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update Material", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getAllMaterials(@Nullable String materialId) {
		Optional<List<?>> optional;
		if(materialId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Material, ObjectFilters.eq("id", materialId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Material);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}

}

