package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.ResourceType;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class ResourceTypeRepo {
	private static final String TAG = ResourceTypeRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertResourceType(ResourceType resourceType) {
		resourceType.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(resourceType);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(resourceType.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save resourceType", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  updateResourceType(ResourceType resourceType) {
		WriteResult result = RepoInteractor.updateEntry(resourceType);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(resourceType.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update resourceType", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getResourceTypesByStatus(@Nullable String status) {
		Optional<List<?>> optional;
		if(status != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.ResourceType, ObjectFilters.eq("active", status.equalsIgnoreCase("active")));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.ResourceType);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static ResourceType getActualResourceTypeById(Long resourceTypeId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.ResourceType, ObjectFilters.eq("id", resourceTypeId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (ResourceType)result.get(0) : null;
	}
}
