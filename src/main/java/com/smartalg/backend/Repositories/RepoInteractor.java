package com.smartalg.backend.Repositories;

import java.util.List;
import java.util.Optional;

import org.dizitart.no2.FindOptions;
import org.dizitart.no2.SortOrder;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.ObjectFilter;
import org.dizitart.no2.objects.ObjectRepository;

import com.smartalg.backend.App;

import io.reactivex.annotations.NonNull;

public class RepoInteractor {
	
	public static WriteResult setEntry(@NonNull final Object object) {
		
		return getRepository(object).insert(object);
	}
	
	public static WriteResult updateEntry(@NonNull final Object object) {
		
		return getRepository(object).update(object);
	}
	
	public static WriteResult setEntries(@NonNull final Object[] list, @NonNull final OBJECT_TYPE object_type) {
		
		return getRepository(object_type).insert(list);
	}
	
	public static Optional<List<?>> getAllEntries(@NonNull final OBJECT_TYPE object_type) {
		return Optional.ofNullable(getRepository(object_type).find().toList());
	}
	
	public static Optional<List<?>> getAllEntries(@NonNull final OBJECT_TYPE object_type, ObjectFilter filter) {
		return Optional.ofNullable(getRepository(object_type).find(filter).toList());
	}
	
	public static WriteResult deleteEntry(@NonNull final OBJECT_TYPE object_type, ObjectFilter filter) {
		return getRepository(object_type).remove(filter);
	}
	
	
	private static ObjectRepository getRepository(@NonNull Object object){
		final OBJECT_TYPE type = object instanceof OBJECT_TYPE? (OBJECT_TYPE) object : OBJECT_TYPE.valueOf(object.getClass().getSimpleName());
		
		switch(type) {
		case Project:
			return App.getDBInstance().getProjectRepo();
		case Section:
			return App.getDBInstance().getSectionRepo();
		case Activity:
			return App.getDBInstance().getActivityRepo();
		case Description:
			return App.getDBInstance().getDescriptionRepo();
		case Resource:
			return App.getDBInstance().getResourceRepo();
		case ResourceType:
			return App.getDBInstance().getResourceTypeRepo();
		case UnitOfMeasure:
			return App.getDBInstance().getUnitOfMeasureRepo();
		case GlobalDecision:
			return App.getDBInstance().getGlobalDecisionRepo();
		case Plant:
			return App.getDBInstance().getPlantRepo();
		case SkilledPerson:
			return App.getDBInstance().getSkilledPersonRepo();
		case Fuel:
			return App.getDBInstance().getFuelRepo();
		case Material:
			return App.getDBInstance().getMaterialRepo();
		default:
			return null;
		}
	}
	
	public enum OBJECT_TYPE {
		Project, Section, Activity, Description, Resource, ResourceType, UnitOfMeasure, GlobalDecision, Plant, SkilledPerson, Fuel, Material
	}
}
