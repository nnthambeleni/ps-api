package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Activity;
import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;

public class ActivityRepo {
	private static final String TAG = ActivityRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertActivity(Activity activity) {
		activity.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(activity);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(activity.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save activity", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Activity insertActualActivity(Activity activity) {
		activity.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(activity);
    	if(result != null && result.getAffectedCount() > 0) {
    		return activity;
    	}else {
    		return null;
    	}
	}
	
	public static ResponseEntity<?>  getAllActivities(@Nullable String sectionId) {
		Optional<List<?>> optional;
		if(sectionId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Activity, ObjectFilters.eq("section.sectionId", sectionId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Activity);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static Activity getActualActivityById(Long activityId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Activity, ObjectFilters.eq("id", activityId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Activity)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  getActivityById(Long activityId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Activity, ObjectFilters.eq("id", activityId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		if(result.size() > 0) {
			return new ResponseEntity<>((Activity)result.get(0), HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Activity not found", HttpStatus.NOT_FOUND);
		}
	
		
	}
	
	public static ResponseEntity<?>  updateActivity(Activity activity) {
		WriteResult result = RepoInteractor.updateEntry(activity);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(activity.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update activity", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
}
