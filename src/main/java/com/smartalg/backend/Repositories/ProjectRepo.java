package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.ObjectFilter;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Project;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;

public class ProjectRepo {
	private static final String TAG = ProjectRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertProject(Project project) {
		project.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(project);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(project.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save project", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  updateProject(Project project) {
		WriteResult result = RepoInteractor.updateEntry(project);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(project.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update project", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getProjectsByStatus(String status, @Nullable Long creator_id) {
		ObjectFilter filter;
    	if(creator_id != null) {
        	filter = ObjectFilters.and(ObjectFilters.eq("active", status.equalsIgnoreCase("active")), ObjectFilters.eq("owner.id", creator_id));
    	}else
    		filter = ObjectFilters.eq("active", status.equalsIgnoreCase("active"));
    	
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Project, filter);
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static ResponseEntity<?>  getAllProjects(@Nullable Long creator_id) {
		Optional<List<?>> optional;
		if(creator_id != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Project, ObjectFilters.eq("owner.id", creator_id));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Project);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}
	
	public static Project getActualProjectById(Long project_id) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Project, ObjectFilters.eq("id", project_id));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Project)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  deleteAllProjects() {
		WriteResult result = RepoInteractor.deleteEntry(RepoInteractor.OBJECT_TYPE.Project, ObjectFilters.ALL);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(true, HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to delete bookings", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
}

