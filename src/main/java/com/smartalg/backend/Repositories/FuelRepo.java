package com.smartalg.backend.Repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.dizitart.no2.NitriteId;
import org.dizitart.no2.WriteResult;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.smartalg.backend.Models.Database.Fuel;
import com.smartalg.backend.Models.Local.ListResponse;

import io.reactivex.annotations.Nullable;


public class FuelRepo {
	private static final String TAG = FuelRepo.class.getSimpleName();
	
	public static ResponseEntity<?> insertFuel(Fuel fuel) {
		fuel.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(fuel);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(fuel.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to save Fuel", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static Fuel insertActualFuel(Fuel Fuel) {
		Fuel.setId(NitriteId.newId().getIdValue());
    	WriteResult result = RepoInteractor.setEntry(Fuel);
    	if(result != null && result.getAffectedCount() > 0) {
    		return Fuel;
    	}else {
    		return null;
    	}
	}
	
	public static Fuel getActualFuelById(Long fuelId) {
		Optional<List<?>> optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Fuel, ObjectFilters.eq("id", fuelId));
		
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return result.size() > 0 ? (Fuel)result.get(0) : null;
	}
	
	public static ResponseEntity<?>  updateFuel(Fuel fuel) {
		WriteResult result = RepoInteractor.updateEntry(fuel);
    	if(result != null && result.getAffectedCount() > 0) {
    		return new ResponseEntity<>(fuel.toString(), HttpStatus.OK);
    	}else {
    		return new ResponseEntity<>("Failed to update Fuel", HttpStatus.INTERNAL_SERVER_ERROR);
    	}
	}
	
	public static ResponseEntity<?>  getAllFuels(@Nullable String fuelId) {
		Optional<List<?>> optional;
		if(fuelId != null) {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Fuel, ObjectFilters.eq("id", fuelId));
		}else {
			optional = RepoInteractor.getAllEntries(RepoInteractor.OBJECT_TYPE.Fuel);
		}
		List<?> result =  optional.isPresent()? optional.get() : new ArrayList<>();
		return new ResponseEntity<>(new ListResponse(result).toString(), HttpStatus.OK);
	}

}

